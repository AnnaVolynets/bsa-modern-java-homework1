package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {
    private String name;
    private PositiveInteger powerGridConsumption;
    private PositiveInteger capacitorConsumption;
    private PositiveInteger optimalSpeed;
    private PositiveInteger optimalSize;
    private PositiveInteger baseDamage;

    public AttackSubsystemImpl(String name, PositiveInteger powerGridConsumption, PositiveInteger capacitorConsumption,
                               PositiveInteger optimalSpeed, PositiveInteger optimalSize,
                               PositiveInteger baseDamage) {
        this.name = name;
        this.powerGridConsumption = powerGridConsumption;
        this.capacitorConsumption = capacitorConsumption;
        this.optimalSpeed = optimalSpeed;
        this.optimalSize = optimalSize;
        this.baseDamage = baseDamage;
    }

    public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
                                                PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
                                                PositiveInteger baseDamage) throws IllegalArgumentException {
        if (name.trim().isEmpty() || name == null) {
            throw new IllegalArgumentException("Name should be not null and not empty");
        }
        return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
                baseDamage);
    }

    @Override
    public PositiveInteger getPowerGridConsumption() {
        return powerGridConsumption;
    }

    @Override
    public PositiveInteger getCapacitorConsumption() {
        return capacitorConsumption;
    }

    @Override
    public PositiveInteger attack(Attackable target) {
        PositiveInteger sizeReductionModifier;
        PositiveInteger speedReductionModifier;

        if (target.getSize().value() == 0) return new PositiveInteger(0);

        if (target.getSize().equals(optimalSize) && target.getCurrentSpeed().value() > optimalSpeed.value()) {
            return new PositiveInteger(baseDamage.value() / 2);
        }
        if ((target.getSize().value() == optimalSize.value() / 2) && (target.getCurrentSpeed().value() == optimalSpeed.value() / 2)) {
            return new PositiveInteger(baseDamage.value() / 2);
        }
        if (target.getSize().value() >= this.optimalSize.value()) {
            sizeReductionModifier = new PositiveInteger(1);
        } else {
            sizeReductionModifier = new PositiveInteger(target.getSize().value() / optimalSize.value());
        }

        if (target.getCurrentSpeed().value() >= this.optimalSpeed.value()) {
            speedReductionModifier = new PositiveInteger(1);
        } else {
            speedReductionModifier = new PositiveInteger(target.getCurrentSpeed().value() / (optimalSpeed.value() * 2));
        }

        return new PositiveInteger(baseDamage.value() * sizeReductionModifier.min(speedReductionModifier).value());
    }

    @Override
    public String getName() {
        return name;
    }

}
