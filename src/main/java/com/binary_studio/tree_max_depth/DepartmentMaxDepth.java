package com.binary_studio.tree_max_depth;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		Integer depth = 1;
		if(rootDepartment == null){return 0;}
		else if(rootDepartment.subDepartments.isEmpty()){
			return depth;
		} else {
			while (!findMax(rootDepartment).equals(rootDepartment)){
				depth++;
				rootDepartment = findMax(rootDepartment);
			}
			for (Department d: rootDepartment.subDepartments) {
				if (d != null) {
					depth++;
				}
			}
		}
		return depth;
	}

	private static Department findMax(Department r){
		Integer max = 0;
		for (Department d: r.subDepartments){
				if (d != null &&d.subDepartments.size() > max ) {
					max = d.subDepartments.size();
					r = d;
				}
		}
		return r;
	}

}

